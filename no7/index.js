const express = require('express')
const app = express()
const Json2csvParser = require('json2csv').Parser;
const fields = ['invoices_id', 'sku_no', 'total_price'];
var csv = require('csv-parser')
var fs = require('fs')
var myData = [];
var report_sku = [];
var total = [];
var invoices_id = [];
var sku_no = [];


 
fs.createReadStream('invoice_data.csv')
  .pipe(csv())
  .on('data', function (data) {
  	total = ((data.base_price - data.discount) + (data.tax + data.shipping_cost)) * data.qty;
  	invoices_id = parseInt(data.invoices_id);
  	sku_no = data.sku_no;

  	sku_no.replace('\'','');
  	myData = [{
	  			"invoices_id" : invoices_id,
	  			"sku_no" : sku_no,
	  			"total_price" : total
		  	}];

	var json2csvParser = new Json2csvParser({ fields,delimiter: '|' });
	report_sku = json2csvParser.parse(myData);	
	console.log(report_sku);  	
   
  })

	app.get('/',function(req,res) {
		console.log(report_sku);
		res.send(report_sku);
	})

	 
	

app.listen(3000,() => console.log('Listening port 3000'))