-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 18, 2018 at 10:37 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bizzy_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(5) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`) VALUES
(1, 'wahyu', 'wahyu'),
(2, 'gudang', 'gudang');

-- --------------------------------------------------------

--
-- Table structure for table `bayar`
--

CREATE TABLE `bayar` (
  `id_bayar` int(5) NOT NULL,
  `id_order` int(5) NOT NULL,
  `judul` varchar(250) DEFAULT NULL,
  `userName` varchar(20) NOT NULL,
  `foto_transfer` varchar(250) DEFAULT NULL,
  `keterangan` text,
  `tanggal_bayar` date DEFAULT NULL,
  `status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bayar`
--

INSERT INTO `bayar` (`id_bayar`, `id_order`, `judul`, `userName`, `foto_transfer`, `keterangan`, `tanggal_bayar`, `status`) VALUES
(4, 20, 'konfirmasi transfer', 'sukmo', 'Screenshot_1.jpg', 'gan sudah ditransfer ', '2015-09-16', 1),
(8, 25, 'udah ditransfer gan', 'jokowi', 'arus data.jpg', 'segera dikirim', '2015-09-18', 1),
(9, 26, 'sidiasda', 'suro', 'dc.jpg', 'asaassda', '2015-09-18', 1),
(10, 27, 'asddfasf', 'suro', 'dc.jpg', 'dsafasff', '2015-09-18', 1),
(11, 28, 'afaf', 'suro', 'arus data.jpg', 'sacfasc', '2015-09-18', 1),
(12, 29, 'sudah dikirim', 'eka', 'dc.jpg', 'cepat dikirim', '2015-09-18', 0),
(13, 30, 'fafaj', 'joko', 'arus data.jpg', 'skafnlank', '2015-09-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `halaman`
--

CREATE TABLE `halaman` (
  `id_halaman` int(5) NOT NULL,
  `judul_halaman` varchar(250) NOT NULL,
  `isi_halaman` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `halaman`
--

INSERT INTO `halaman` (`id_halaman`, `judul_halaman`, `isi_halaman`) VALUES
(1, 'Hubungi Kami', 'Silahkan hubungi kami melalui \r\nHP : 0898127121<br>\r\nEmail : info@cahyacyber.co.id'),
(2, 'Cara Pembayaran', 'Pembeli setelah melakukan pemesanan barang dapat melakukan pembayaran melalui transfer bank, melalui ATM.\r\nUntuk pembayaran melalui:\r\nBank BCA\r\nBCA\r\nKCP Kota metro\r\nNo. Rek. : 5035 010201\r\n\r\nBank Mandiri\r\nBANK MANDIRI\r\nKCP Kota metro\r\nNo. Rek. : 070-00-0500364-0\r\nAtas Nama CV.CMMandiri. \r\nPembayaran harus dilakukan secara penuh .\r\nSemua biaya yang tercantum menggunakan mata uang Rupiah Indonesia.\r\nPesanan akan diproses setelah CV. Cahya Mulya Mandiri menerima pembayaran.\r\nSetelah melakukan proses transfer, mohon konfirmasikan transfer dengan mengirimkan bukti pembayaran.');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_barang`
--

CREATE TABLE `kategori_barang` (
  `id_kategori` int(5) NOT NULL,
  `nama_kategori` varchar(250) NOT NULL,
  `foto_kategori` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_barang`
--

INSERT INTO `kategori_barang` (`id_kategori`, `nama_kategori`, `foto_kategori`) VALUES
(1, 'Laptop', 'laptop.jpg'),
(2, 'Komputer', 'komputer.jpg'),
(3, 'Printer', NULL),
(4, 'Alat komputer', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `list_barang`
--

CREATE TABLE `list_barang` (
  `id_barang` int(5) NOT NULL,
  `id_kategori` int(5) NOT NULL,
  `nama_barang` varchar(250) NOT NULL,
  `foto_barang` varchar(250) DEFAULT NULL,
  `deskripsi_barang` text,
  `stok` int(5) NOT NULL,
  `tanggal_input` date DEFAULT NULL,
  `harga_barang` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `list_barang`
--

INSERT INTO `list_barang` (`id_barang`, `id_kategori`, `nama_barang`, `foto_barang`, `deskripsi_barang`, `stok`, `tanggal_input`, `harga_barang`) VALUES
(1, 1, 'Acer Aspire One', '15lenovo-9079-25597-1-zoom.jpg', 'Dijual acer', 1, '2015-09-10', '5000000'),
(2, 2, 'Asus K430 I', '56asus.jpg', 'Asus Ram 1 GB Hardisk 90GB', 2, '2015-09-13', '5000000'),
(3, 3, 'Asus K430 A', '64asus-9605-88418-1-.jpg', 'Asus Murah', 2, '2015-09-13', '9000000'),
(4, 1, 'Asus A435', '56asus.jpg', 'Asus', 6, '2015-09-13', '5000000'),
(5, 2, 'Lenovo', '15lenovo-9079-25597-1-zoom.jpg', 'Lenovo', 10, '2015-09-13', '7000000'),
(6, 3, 'Acer ', 'gambarkosong.jpg', 'Acewr', 7, '2015-09-13', '8000000'),
(7, 3, 'Acer Aspire One', 'small_15lenovo-9079-25597-1-zoom.jpg', NULL, 5, NULL, '9000000'),
(8, 3, 'Printer Cannon', '76Epson L210 (32).jpg', 'Printer', 85, NULL, '500000'),
(9, 3, '1212891', 'small_15lenovo-9079-25597-1-zoom.jpg', 'jsakjsa', 89, '2015-09-13', '9000'),
(10, 3, 'Printer Epson', '752.jpg', 'Printer Epson', 7, '2015-09-13', '800.000'),
(11, 4, 'Pembersih Laptop', '11185441_763878333709729_1986222637_n.jpg', 'Pembersih', 1, '2015-09-13', '60000');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(5) NOT NULL,
  `userName` varchar(20) NOT NULL,
  `passPass` varchar(20) NOT NULL,
  `nama_lengkap` varchar(30) DEFAULT NULL,
  `foto_profil` varchar(250) DEFAULT NULL,
  `alamat` text,
  `email` varchar(100) DEFAULT NULL,
  `no_hp` int(12) DEFAULT NULL,
  `tanggal_daftar` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `userName`, `passPass`, `nama_lengkap`, `foto_profil`, `alamat`, `email`, `no_hp`, `tanggal_daftar`) VALUES
(1, 'Kos', 'kostumer', 'kostumer', 'kostumer.jpg', 'kostumer', 'kostumer@gmail.com', 817621211, '2015-09-09'),
(2, 'joni', 'masuk123', 'Joni Iskandar', 'bePn4lUUtTkhF-N41P3d6pZQFbL4oW0dDHH37ZwRIoo.png', 'Joni Jalan jalan', 'joni@gmail.com', 2147483647, '2015-09-15'),
(3, 'Dedi', 'masuk123', 'Dedi', 'Screenshot_1.jpg', 'hashajs', 'j@aska.com', 1929812911, '2015-09-15'),
(4, 'monyet', 'monyet', 'Monyet', 'aneh.jpg', 'Monyet', 'moynyr@gmail.com', 2147483647, '2015-09-15'),
(5, '', '', 'sukoco', 'veby.jpg', 'ahsaks', 'in@mas.com', 2903232, '2015-09-15'),
(6, 'susu', 'susu', 'susu', 'baby.jpg', 'susu', 'susu@gmail.com', 2147483647, '2015-09-15'),
(7, 'wahyu', 'wahyu', 'Wahyu Sukmo', 'Screenshot_4.jpg', 'aus', 'aas@gmail.com', 99899233, '2015-09-15'),
(8, 'sukmo', 'sukmo', 'sukmo', 'no-image.jpg', 'sukmo', 'sukmo@hs.com', 283921372, '2015-09-16'),
(9, 'jokowi', 'masukin', 'jokowidodo', 'no-image.jpg', 'jakarta', 'lalaal@sfs.com', 98721, '2015-09-18'),
(10, 'suro', 'masukin', 'surodi', 'no-image.jpg', 'jalan ahmad yani', 'suro@gmail.com', 876262722, '2015-09-18'),
(11, 'eka', '12345', 'eka aja', 'no-image.jpg', 'lampung', 'eka@gmail.com', 983525323, '2015-09-18'),
(12, 'joko', 'masuk123', 'ari aja', 'no-image.jpg', NULL, 'afaskfl@com', 9240104, '2015-09-18');

-- --------------------------------------------------------

--
-- Table structure for table `user_order`
--

CREATE TABLE `user_order` (
  `id_order` int(5) NOT NULL,
  `user_id` int(20) NOT NULL,
  `id_barang` int(5) NOT NULL,
  `jumlah_order` int(5) NOT NULL,
  `tanggal_order` date NOT NULL,
  `jam_order` time DEFAULT NULL,
  `status_order` int(1) NOT NULL,
  `total_bayar` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_order`
--

INSERT INTO `user_order` (`id_order`, `user_id`, `id_barang`, `jumlah_order`, `tanggal_order`, `jam_order`, `status_order`, `total_bayar`) VALUES
(1, 1, 1, 2, '2015-09-13', '08:15:01', 0, NULL),
(2, 1, 1, 1, '2015-09-13', '08:25:03', 0, NULL),
(3, 1, 1, 1, '2015-09-13', '08:26:16', 0, NULL),
(4, 1, 1, 2, '2015-09-13', '09:17:43', 1, NULL),
(22, 0, 1, 2, '2015-09-16', '09:27:41', 0, '10000000'),
(25, 0, 3, 1, '2015-09-18', '07:06:13', 1, '9000000'),
(26, 0, 4, 1, '2015-09-18', '08:23:51', 1, '5000000'),
(28, 0, 8, 1, '2015-09-18', '09:39:47', 1, '500000'),
(29, 0, 3, 1, '2015-09-18', '10:09:28', 0, '9000000'),
(30, 0, 8, 2, '2015-09-18', '11:00:29', 0, '1000000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `bayar`
--
ALTER TABLE `bayar`
  ADD PRIMARY KEY (`id_bayar`);

--
-- Indexes for table `halaman`
--
ALTER TABLE `halaman`
  ADD PRIMARY KEY (`id_halaman`);

--
-- Indexes for table `kategori_barang`
--
ALTER TABLE `kategori_barang`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `list_barang`
--
ALTER TABLE `list_barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `user_order`
--
ALTER TABLE `user_order`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `id_barang` (`id_barang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bayar`
--
ALTER TABLE `bayar`
  MODIFY `id_bayar` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `halaman`
--
ALTER TABLE `halaman`
  MODIFY `id_halaman` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kategori_barang`
--
ALTER TABLE `kategori_barang`
  MODIFY `id_kategori` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `list_barang`
--
ALTER TABLE `list_barang`
  MODIFY `id_barang` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user_order`
--
ALTER TABLE `user_order`
  MODIFY `id_order` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kategori_barang`
--
ALTER TABLE `kategori_barang`
  ADD CONSTRAINT `kategori_barang_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `list_barang` (`id_barang`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
