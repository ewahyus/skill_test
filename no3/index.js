const express = require('express')
const app = express()

var A = [1,2,5,7];
var B = [3,4,8];
var C = A.concat(B);





function selectionSort(items) {
	var length = items.length;
	for (var i = 0; i < length - 1; i++) {
		//Number of passes
		var min = i;
		for (var j = i + 1; j < length; j++) {
			if (items[j] < items[min]) { 
				min = j; 
			}
		}
		if (min != i) {
			var tmp = items[i];
			items[i] = items[min];
			items[min] = tmp;
		}
	}
	return items;
}

app.get('/',function(req,res) {
	var array = C;
	sorted = selectionSort(array);
	res.send("C => " + sorted);
})

app.listen(3000,() => console.log('Listening port 3000'))