const express = require('express')
const app = express()

var mysql = require('mysql')
var connection = mysql.createConnection({
	host : 'localhost',
	user : 'root',
	password : '',
	database : 'bizzy_test'
});

connection.connect()

var sql = "SELECT COUNT(*) FROM employee WHERE kelamin = 0 AND umur > 35";

var countEmp;
var data;

connection.query(sql,function(req,res){
	countEmp = res;
	data = res;
});


app.get('/',function(req,res) {
	res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({ 
    	numberOfEmployee: countEmp, 
    	employee : data
    }));
})

app.listen(3000,() => console.log('Listening port 3000'))
