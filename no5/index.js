const express = require('express')
const app = express()

var monthNames = {
	"Januari" 	: 1,
	"Februari" 	: 2,
	"Maret" 	: 3,
	"April" 	: 4,
	"Mei" 		: 5,
	"Juni" 		: 6,
	"Juli" 		: 7,
	"Agustus" 	: 8,
	"September" : 9,
	"Oktober" 	: 10,
	"November" 	: 11,
	"Desember" 	: 12
};

var data = [
	["April"],
	["Februari"],
	["Agustus"],
	["Maret"]
];

data.sort(function(a,b){
	return monthNames[a[0]] - monthNames[b[0]];
});

app.get('/',function(req,res) {
	res.send(JSON.stringify(data));
})

app.listen(3000,() => console.log('Listening port 3000'))